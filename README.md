# Helm CHart for Monitoring

## Prerequisites

This project requires you to have knowledge about the following tools needed for local development:
    - Docker 
    - [kubectl](https://kubernetes.io/docs/reference/kubectl) 
    - [kind](https://kind.sigs.k8s.io)
    - [helm](https://helm.sh)

 On macOS, execute:

```sh
brew install --cask docker
brew install kubectl kind helm
```

## Setup and installation

### Create the k8s cluster based on KinD

```sh
# create and check the cluster
kind create cluster --config kind-cluster-config.yaml
kubectl cluster-info --context kind-kind
```

### Build helm dependencies and install the chart

```sh
# ensure dependencies are available
helm dependencies build

# install the umbrella chart
helm install monitoring ./helm-chart

# [optional] if you previously created a namespace, then do it like this
helm install monitoring ./helm-chart --namespace monitoring
```

### Access Grafana locally

```sh
export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=grafana,app.kubernetes.io/instance=monitoring" -o jsonpath="{.items[0].metadata.name}")
export CONTAINER_PORT=$(kubectl get pod --namespace monitoring $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")

echo "Visit http://127.0.0.1:8002 to use your application"
kubectl --namespace monitoring port-forward $POD_NAME 8002:$CONTAINER_PORT
```
### Optionally, deploy and access the [k8s dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard)

```sh
# Go to https://github.com/kubernetes/dashboard to check for the latest version
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml

# setup a service account and clusterrole binding
kubectl apply -f service-admin-account.yaml

# create a bearer token and copy the output of the command
kubectl -n kubernetes-dashboard create token admin-user

# [optional] if you want to separate this from other running deployments, create a namespaces
kubectl create namespace monitoring

# start a proxy gateway between k8s and localhost
kubectl proxy
```

Access the Admin Dashboard via: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

## Cleanup
If you want to uninstall a release from Kubernetes, then simply run `helm uninstall <name of release>`.

In case you want to delete the full cluster, you can run `kind delete cluster`. In case you create a cluster with a name, then run `kind delete cluster --name <name of cluster>`.

## Additional Reading

Kubernetes (+ Dashboard)
* https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md
* https://github.com/imorti/kind-dashboard-setup
* https://medium.com/@munza/local-kubernetes-with-kind-helm-dashboard-41152e4b3b3d

Helm
* https://medium.com/@munza/local-kubernetes-with-kind-helm-dashboard-41152e4b3b3d
* https://github.com/ActianCorp/integration-manager-umbrella-helm-chart
