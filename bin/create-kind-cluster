#!/usr/bin/env sh

SPACER="\n# **************************************************************************\n#"

NAMESPACE=monitoring
RELEASE=monitoring

while getopts "hn:r:" option; do
  case $option in
    h)
      echo "usage: $0 [<options>]"
      echo ""
      echo "  -n <namespace>  Namespace to be created (default: $NAMESPACE)"
      echo "  -r <release>    Release of the installed chart (default: $RELEASE)"
      exit;;
    n) NAMESPACE=$OPTARG;;
    r) RELEASE=$OPTARG;;
  esac
done

echo $SPACER
echo "# Building helm dependencies. If you want to update, run:"
echo "#   helm dependency update"
echo "#\n"
helm dependency build ./helm-chart

echo $SPACER
echo "# Creating KinD cluster"
echo "#\n"
kind create cluster --config kind-cluster-config.yaml

echo $SPACER
echo "# Creating namespace \"$NAMESPACE\""
echo "#\n"
kubectl create namespace $NAMESPACE

# ## https://kind.sigs.k8s.io/docs/user/ingress/#ingress-nginx
# kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

echo $SPACER
echo "# Installing helm chart under release \"$RELEASE\""
echo "#\n"
helm install $RELEASE ./helm-chart --namespace $NAMESPACE

echo $SPACER
echo "# To access the Grafana dashboards:"
echo "#   ./bin/grafana-proxy"
echo "#"
echo "# To update a changed chart later:"
echo "#   helm upgrade $RELEASE ./helm-chart --namespace $NAMESPACE"
echo "#\n"
